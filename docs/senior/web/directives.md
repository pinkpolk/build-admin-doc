---
pageClass: max-content
---

# 内置指令

## 按钮鉴权
`v-auth`指令会以当前页面的`path`从服务端获取到的权限节点进行比对，来确定用户是否拥有按钮权限，无权限则隐藏按钮。

> 关于鉴权，您可以还需要了解[路由与权限](https://wonderful-code.gitee.io/senior/addMenuRule.html)

``` vue
<template>
    <el-button v-auth="'add'">添加</el-button>
    <el-button v-auth="'edit'">编辑</el-button>
    <el-button v-auth="'del'">删除</el-button>
</template>
```

## 表格横向滚动
当表格宽度超出屏幕时，`v-table-lateral-drag`指令可以使得鼠标滚轮控制表格的横向滚动条。
``` vue
<template>
    <el-table v-table-lateral-drag>
        <!-- ... -->
    </el-table>
</template>
```

## 点击自动失焦
`el-button`在点击后，不会自动失去焦点，造成按钮颜色异常，所以，我们准备了`v-blur`指令，该指令在点击后，立即将按钮脱焦。
``` vue
<template>
    <el-button v-blur>点击后立即失去焦点</el-button>
</template>
```

## 缩放
`v-zoom`指令使元素支持缩放，传递的参数为需要支持缩放的元素的`css`类名。
``` vue
<template>
    <el-dialog
        custom-class="ba-operate-dialog"
        v-model="dialogVisible"
    >
        <template #title>
            <div class="title" v-zoom="'.ba-operate-dialog'">标题</div>
        </template>
        <div>
            内容
        </div>
    </el-dialog>
</template>

<script lang="ts" setup>
import { ref } from 'vue'
const dialogVisible = ref(true)
</script>
```

## 拖动
`v-drag`使得元素可以被拖动，传递的参数一为被拖动元素，参数二表示拖动`句柄`元素，仅在`句柄`元素内，可以进行拖动（比如只在弹出的标题范围内可以拖动整个弹窗）。
``` vue
<template>
    <el-dialog
        custom-class="ba-operate-dialog"
        v-model="dialogVisible"
    >
        <template #title>
            <div class="title" v-drag="['.ba-operate-dialog', '.el-dialog__header']">标题</div>
        </template>
        <div>
            内容
        </div>
    </el-dialog>
</template>

<script lang="ts" setup>
import { ref } from 'vue'
const dialogVisible = ref(true)
</script>
```