---
pageClass: max-content
---

# 字体图标
我们封装了一个功能齐全的图标选择器\
![](/images/web/icon/web-icon1.gif)\
在这个图标选择器中，支持：`element-plus`、`font-awesome`、`阿里iconfont`、`本地SVG`这四种图标的快速输入，同时的，我们封装了一个`Icon`组件，以同时支持这四种图标的显示。

## 全局Icon组件
- 组件支持`四种`图标。
- `四种`图标均支持 color 和 size 属性，使用示例见下文：
``` vue
# font-awesome的图标，使用 fa 作为前缀（带个空格）
<Icon name="fa fa-pencil" />

# element-plus 的图标，使用 el-icon 作为前缀，图标名称请使用：首字母大写的驼峰语法
<Icon name="el-icon-Close" color="#8595F4" size="20" />

# 本地`/web/src/assets/icons`文件夹内的SVG图标，使用 local 作为前缀，文件名作为后缀
# 文件自动加载，新增请重新编译
<Icon name="local-logo" />

# 阿里iconfont的图标，使用 iconfont 作为前缀（带个空格）
<Icon name="iconfont icon-user" size="20" />
```
有时，`Icon`组件会覆盖不了图标的原有颜色或大小，请检查`SVG`文件的源代码。

## 图标添加与配置

### 本地图标
1. 将`SVG`图标文件，放入本地`/web/src/assets/icons`文件夹，然后重新编译项目。
2. 系统会自动加载该目录下的所有图标文件备用，现在你可以使用`Icon`组件来显示图标了。
3. 示例：`<Icon name="local-SVG的文件名" />`
4. 不建议在此文件夹放置`非常大、非常多`的文件，可能会影响系统加载速度，当确有此方面需求时，请将文件放置到其他位置，并单独导入和使用图标。

### font-awesome 图标
1. 此图标库目前`BuildAdmin`已经默认加载。加载代码在`/web/src/utils/iconfont.ts`中的`cssUrls`中。
2. 你可以直接使用`font-awesome 4.7.0`的所有图标，这些图标可以在[这里](http://www.fontawesome.com.cn/faicons/)找到，当然你也可以直接使用图标选择器寻找图标。
3. 示例：`<Icon name="fa fa-pencil" />`，注意图标名称一定是以`fa`加一个空格开头。

### element-plus 图标
1. 此图标库目前`BuildAdmin`已经默认加载，加载代码在`/web/src/main.ts`执行的`registerIcons`方法中。
2. 你可以直接使用`element-plus/icons-vue ~1.1.4`的所有图标，在使用`element-plus`图标时，请使用`el-icon`作为前缀，图标名称请使用：首字母大写的驼峰语法。
3. 示例：`<Icon name="el-icon-RefreshRight" />`

### iconfont 图标
1. 目前系统未使用任何`iconfont`图标，你可以[获取iconfont图标库项目的Font class链接](/senior/web/icon.html#获取iconfont图标库项目的font-class链接)后，设置到`/web/src/utils/iconfont.ts`中的`cssUrls`中，系统会自动加载链接中的所有图标以供使用。
2. 示例：`<Icon name="iconfont icon-user" />`，注意图标名称一定是以`iconfont`加一个空格开头，以图标名称结尾。

## 图标选择器组件

### 使用 BaInput
``` vue
<template>
    <BaInput type="icon" v-model="state.icon" />

    <!-- 使用大号的输入框 -->
    <BaInput type="icon" v-model="state.icon" :attr="{size: 'large'}" />

    <!-- 禁用状态的图标选择器 -->
    <BaInput type="icon" v-model="state.icon" :attr="{disabled: true}" />

    <!-- 图标选择器面板出现在上方，同时显示选择的图标名称 -->
    <BaInput type="icon" v-model="state.icon" :attr="{placement: 'top', 'show-icon-name': true}" />
</template>

<script setup lang="ts">
import { reactive } from 'vue'
import BaInput from '/@/components/baInput/index.vue'

const state = reactive({
    // Icon默认值
    icon: 'fa fa-drivers-license',
})
</script>
```

### 使用 FormItem
```vue
<template>
    <el-form>
        <!-- 基本使用 -->
        <FormItem label="选择图标" type="icon" v-model="state.icon" />

        <!-- 使用大号的输入框 -->
        <FormItem label="选择图标" type="icon" v-model="state.icon" :input-attr="{size: 'large'}" />

        <!-- 禁用的图标选择器 -->
        <FormItem label="选择图标" type="icon" v-model="state.icon" :input-attr="{disabled: true}" />

        <!-- 图标选择器面板出现在上方，同时显示选择的图标名称 -->
        <FormItem label="选择图标" type="icon" v-model="state.icon" :input-attr="{placement: 'top', 'show-icon-name': true}" />
    </el-form>
</template>

<script setup lang="ts">
import { reactive } from 'vue'
import FormItem from '/@/components/formItem/index.vue'

const state = reactive({
    // Icon默认值
    icon: 'fa fa-drivers-license',
})
</script>
```

### 图标选择器属性参考

若要使用在`BaInput`或`FormItem`上，请按上述示例的格式录入属性
|属性|说明|类型|可选值|默认值|
|:----:|:----:|:----:|:----:|:----:|
|size|输入框大小|string|default/small/large|default|
|disabled|是否禁用|bool|true/false|false|
|title|输入框提示|string||请选择图标|
|type|默认图标类型|string|ele/awe/ali/local|ele|
|v-model/modelValue|绑定值|string||el-icon-Minus|
|show-icon-name|是否显示图标名称|bool|true/false|false|
|placement|选择器出现位置|string|top/top-start/top-end/bottom/bottom-start/bottom-end/left/left-start/left-end/right/right-start/right-end|bottom|

### 获取`iconfont`图标库项目的`Font class`链接
1. 在[iconfont官网](https://www.iconfont.cn/)完成登录，并将一些图标`添加入库`
![](/images/web/icon/web-icon2.png)
2. 点击iconfont网站右上角的`购物车`，点击`添加至项目`，你可以选择`创建`一个新的项目或`加入`到已有项目。\
![](/images/web/icon/web-icon3.png)
3. 进入iconfont网站顶部的资源管理->我的项目内\
![](/images/web/icon/web-icon4.png)
4. 点击复制代码
![](/images/web/icon/web-icon5.png)
5. 粘贴至`/web/src/utils/iconfont.ts`的`cssUrls`中
![](/images/web/icon/web-icon6.png)
6. 重新编译项目。