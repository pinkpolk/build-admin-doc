---
pageClass: max-content
---

# CSS/SCSS 样式

## styles 目录
在`/web/src/styles`目录下，我们定义了多个`scss`文件，几乎所有的公用样式都保存在这些文件中。
|文件|注释|
|:----:|----|
|app.scss|应用样式表（基础样式、框架全局样式）|
|index.scss|导入所有可用样式表，`main.ts`中就可以只加载它了|
|element.scss|对`element plus`原有样式的改写|
|loading.scss|`loading`相关的样式表|
|var.scss|全局`css`变量定义|
|dark.scss|暗黑模式下的`css`变量定义|
|mixins.scss|`scss`mixins与function定义|

## 预设颜色变量
`BuildAdmin`和`Element plus`预设了一些颜色变量，它们的颜色值可自动兼容暗黑模式。

### BuildAdmin（全部）
|变量名称|默认值|暗黑值|
|:----:|----|----|
|--ba-color-primary-light|<Color color='#3f6ad8' /> #3F6AD8|
|--ba-bg-color|<Color color='#f5f5f5' /> #F5F5F5|<Color color='#141414' /> #141414
|--ba-bg-color-overlay|<Color color='#ffffff' /> #FFFFFF|<Color color='#1d1e1f' /> #1D1E1F
|--ba-border-color|<Color color='#f6f6f6' /> #F6F6F6|<Color color='#58585b' /> #58585B

### Element plus（部分）
|变量名称|默认值|暗黑值|
|:----:|----|----|
|--el-color-white|<Color color='#ffffff' /> #FFFFFF|
|--el-color-black|<Color color='#000000' /> #000000|
|--el-text-color-primary|<Color color='#303133' /> #303133|<Color color='#E5EAF3' /> #E5EAF3
|--el-text-color-regular|<Color color='#606266' /> #606266|<Color color='#CFD3DC' /> #CFD3DC
|--el-text-color-secondary|<Color color='#909399' /> #909399|<Color color='#A3A6AD' /> #A3A6AD
|--el-text-color-placeholder|<Color color='#a8abb2' /> #A8ABB2|<Color color='#8D9095' /> #8D9095
|--el-text-color-disabled|<Color color='#c0c4cc' /> #C0C4CC|<Color color='#6C6E72' /> #6C6E72
|--el-color-primary|<Color color='#409eff' /> #409EFF|
|--el-color-primary-light-3|<Color color='#79bbff' /> #79BBFF|<Color color='#3375b9' /> #3375B9
|--el-color-primary-light-5|<Color color='#a0cfff' /> #A0CFFF|<Color color='#2a598a' /> #2A598A
|--el-color-primary-light-7|<Color color='#c6e2ff' /> #C6E2FF|<Color color='#213d5b' /> #213D5B
|--el-color-primary-light-8|<Color color='#d9ecff' /> #D9ECFF|<Color color='#1d3043' /> #1D3043
|--el-color-primary-light-9|<Color color='#ecf5ff' /> #ECF5FF|<Color color='#18222c' /> #18222C
|--el-color-primary-dark-2|<Color color='#337ecc' /> #337ECC|<Color color='#66b1ff' /> #66B1FF
|--el-color-success|<Color color='#67c23a' /> #67C23A|
|--el-color-success-light-3|<Color color='#95d475' /> #95D475|<Color color='#4e8e2f' /> #4E8E2F
|--el-color-success-light-5|<Color color='#b3e19d' /> #B3E19D|<Color color='#3e6b27' /> #3E6B27
|--el-color-success-light-7|<Color color='#d1edc4' /> #D1EDC4|<Color color='#2d481f' /> #2D481F
|--el-color-success-light-8|<Color color='#e1f3d8' /> #E1F3D8|<Color color='#25371c' /> #25371C
|--el-color-success-light-9|<Color color='#f0f9eb' /> #F0F9ED|<Color color='#1c2518' /> #1C2518
|--el-color-success-dark-2|<Color color='#529b2e' /> #529B2E|<Color color='#85ce61' /> #85CE61
|--el-color-warning|<Color color='#e6a23c' /> #E6A23C|
|--el-color-warning-light-3|<Color color='#eebe77' /> #EEBE77|<Color color='#a77730' /> #A77730
|--el-color-warning-light-5|<Color color='#f3d19e' /> #F3D19E|<Color color='#7d5b28' /> #7D5B28
|--el-color-warning-light-7|<Color color='#f8e3c5' /> #F8E3C5|<Color color='#533f20' /> #533F20
|--el-color-warning-light-8|<Color color='#faecd8' /> #FAECD8|<Color color='#3e301c' /> #3E301C
|--el-color-warning-light-9|<Color color='#fdf6ec' /> #FDF6EC|<Color color='#292218' /> #292218
|--el-color-warning-dark-2|<Color color='#b88230' /> #B88230|<Color color='#ebb563' /> #EBB563
|--el-color-danger|<Color color='#f56c6c' /> #F56C6C|
|--el-color-danger-light-3|<Color color='#f89898' /> #F89898|<Color color='#b25252' /> #B25252
|--el-color-danger-light-5|<Color color='#fab6b6' /> #FAB6B6|<Color color='#854040' /> #854040
|--el-color-danger-light-7|<Color color='#fcd3d3' /> #FCD3D3|<Color color='#582e2e' /> #582E2E
|--el-color-danger-light-8|<Color color='#fde2e2' /> #FDE2E2|<Color color='#412626' /> #412626
|--el-color-danger-light-9|<Color color='#fef0f0' /> #FEF0F0|<Color color='#2b1d1d' /> #2B1D1D
|--el-color-danger-dark-2|<Color color='#c45656' /> #C45656|<Color color='#f78989' /> #F78989
|--el-color-info|<Color color='#909399' /> #909399|
|--el-color-info-light-3|<Color color='#b1b3b8' /> #B1B3B8|<Color color='#6b6d71' /> #6B6D71
|--el-color-info-light-5|<Color color='#c8c9cc' /> #C8C9CC|<Color color='#525457' /> #525457
|--el-color-info-light-7|<Color color='#dedfe0' /> #DEDFE0|<Color color='#393a3c' /> #393A3C
|--el-color-info-light-8|<Color color='#e9e9eb' /> #E9E9EB|<Color color='#2d2d2f' /> #2D2D2F
|--el-color-info-light-9|<Color color='#f4f4f5' /> #F4F4F5|<Color color='#202121' /> #202121
|--el-color-info-dark-2|<Color color='#73767a' /> #73767A|<Color color='#a6a9ad' /> #A6A9AD

## 自定义样式变量

`var.scss`
``` scss
$vars: () !default;
$vars: map.merge(
    (
        'color-1': #F5F7FA, // css变量名：--ba-vars-color-1
        'color-2': #FAFAFA, // css变量名：--ba-vars-color-2
    ),
    $vars
);

// 批量注册css变量，自动加 --ba-vars-前缀
@include set-component-css-var('vars', $vars);

// 单独定义一个css变量
$var: #ffffff;
@include set-css-var-value('var-color', $var); // css变量名：--ba-var-color
@include set-css-var-value('var-color-light', '#f2f2f2');
```

`dark.scss` 暗黑模式下的颜色值（变量名应和`var.scss`文件中的同步）。
``` scss
$vars: () !default;
$vars: map.merge(
    (
        'color-1': #262727,
        'color-2': #1D1D1D,
    ),
    $vars
);

@include set-component-css-var('vars', $vars);

// 单独定义一个css变量
$var: #000000;
@include set-css-var-value('var-color', $var);
```

## 如何快速查找可用变量
打开项目，然后在`styles`面板搜索`--ba`或`--el`来查看已有的样式变量。

![](/images/web/styles-1.png)
