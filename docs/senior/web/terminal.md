---
pageClass: max-content
---

# WEB 终端

- 我们内置了一个 WEB 终端以实现一些理想中的功能，此终端，后续将在系统中发挥及其关键的作用。
- 终端被划分为安装程序的部分，使用终端前，请先[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#启动安装服务)。

您可以在任何页面管理终端组件的所有数据：

``` vue
<script setup lang="ts">
import { useTerminal } from "/@/stores/terminal"

const terminal = useTerminal()

// 显示终端窗口，不填写参数则表示通过当前的状态来切换显示/隐藏
terminal.toggle(true)

// 执行一个命令
terminal.addTask("version-view.npm")

// 通过当前的包管理器执行一个命令
terminal.addTaskPM("web-install")

// 在后台的终端按钮上显示/隐藏一个红点
terminal.toggleDot()

// 包管理器选择窗口显示/隐藏
terminal.togglePackageManagerDialog()

// 修改包管理器
terminal.changePackageManager("pnpm")

// ...请参考 /@/stores/terminal.ts 文件获取更多可用方法
</script>
```
