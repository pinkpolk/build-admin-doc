---
pageClass: max-content
---

# 辅助工具/函数

## 加载网络CSS/JS文件
``` ts
import { loadCss, loadJs } from '/@/utils/common'
loadCss('网络css文件url')
loadJs('网络js文件url')
```

## 设置页面标题
``` ts
import { setTitle } from '/@/utils/common'
setTitle('新的页面标题')
```

## 检查URL是否是外部的
``` ts
import { isExternal } from '/@/utils/common'
if (isExternal('路径')) {
    console.log('是外部链接')
} else {
    console.log('不是外部链接')
}
```

## 获取管理员身份令牌
``` ts
import { useAdminInfo } from '/@/stores/adminInfo'
const adminInfo = useAdminInfo()
console.log(adminInfo.getToken())
console.log(adminInfo.getToken('refresh')) // token 刷新令牌
```

## 获取会员身份令牌
``` ts
import { useUserInfo } from '/@/stores/userInfo'
const userInfo = useUserInfo()
console.log(userInfo.getToken())
console.log(userInfo.getToken('refresh')) // token 刷新令牌
```

## 防抖

`debounce`函数，在`间隔毫秒数内`重复触发`执行函数`，则只执行一次
- 参数一：执行函数
- 参数二：间隔毫秒数
``` vue
<template>
    <el-input v-model="state.search" @input="debounce(onSearchInput, 500)()" />
</template>

<script>
import { reactive } from 'vue'
import { debounce } from '/@/utils/common'

const state = reactive({
    search: '',
})

const onSearchInput = () => {
    console.log('开始搜索:' + state.search)
}
</script>
```

## 表单重置
``` vue
<template>
    <el-form ref="formRef">
        <el-button @click="onResetForm(formRef)">重置</el-button>
    </el-form>
</template>

<script>
import { onResetForm } from '/@/utils/common'
import { ElForm } from 'element-plus'

const formRef = ref<InstanceType<typeof ElForm>>()
</script>
```

## 是否在后台应用内
``` ts
import { isAdminApp } from '/@/utils/common'
if (isAdminApp()) {
    console.log('在后台')
} else {
    console.log('不在后台')
}
```

## 是否为手机设备
``` ts
import { isMobile } from '/@/utils/common'
if (isMobile()) {
    console.log('用户使用手机访问')
}
```

## 按钮鉴权（函数式）
`auth`函数会以当前页面的`path`从服务端获取到的权限节点进行比对，来确定用户是否拥有某个按钮的权限。
``` ts
import { auth } from '/@/utils/common'
if (auth('add')) {
    console.log('拥有添加权限')
} else {
    console.log('不拥有添加权限')
}
```

## 随机数生成
``` ts
import { randomNum, uuid, shortUuid } from '/@/utils/random'
console.log(randomNum(4,6)) // 生成4到6位的随机数
console.log(shortUuid('前缀')) // 生成唯一标识
console.log(uuid()) // 生成全球唯一标识
```

## 切换路由
通过此函数切换路由时，切换失败，会有错误提示，参数同`router.push`

``` ts
import { routePush } from '/@/utils/router'
routePush({ name: 'routine/adminInfo', params: { a: 1 } })
routePush('/admin/dashboard')

routePush('/test/posva#hash')
routePush({ path: '/test/posva', hash: '#hash' })
routePush({ name: 'test', params: { a: '1' }, hash: '#hash' })
// 只改变 hash
routePush({ hash: '#hash' })
// 只改变 query
routePush({ query: { page: '2' } })
// 只改变 param
routePush({ params: { a: '1' } })
```

## 本地缓存与会话缓存
``` ts
import { Local, Session } from '/@/utils/storage'

Local.set('key', 'value') // 设置本地缓存
Local.get('key') // 获取本地缓存值
Local.remove('key') // 删除某个本地缓存
Local.clear() // 清理所有本地缓存

Session.set('key', 'value') // 设置会话缓存
Session.get('key') // 获取会话缓存值
Session.remove('key') // 删除某个会话缓存
Session.clear() // 清理所有会话缓存
```

## 快速获取当前应用实例
``` ts
import useCurrentInstance from '/@/utils/useCurrentInstance'
const { proxy } = useCurrentInstance()

proxy.eventBus.off('xxx')
```