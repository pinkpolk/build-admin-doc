---
pageClass: max-content
---

# 状态管理

> `BuildAdmin`的状态管理使用了[Pinia](https://pinia.vuejs.org/introduction.html)，在使用前，请对其有基础了解，这是`vue3`官方推荐的状态管理工具。

## stores 目录

`stores`意为状态商店，`BuildAdmin`所有的状态和操作状态的方法，都放在了`\src\stores`目录下。

``` vue
<template>
    <!-- 使用来自状态商店的 adminInfo 的头像 -->
    <img :src="adminInfo.avatar" alt="" />
    <div>当前语言是：{{ config.lang.defaultLang }}</div>
</template>

<script lang="ts" setup>
import { computed } from 'vue'
import { useAdminInfo } from "/@/stores/adminInfo" // 管理员的状态数据
import { useConfig } from '/@/stores/config' // WEB的全局布局配置数据
import { useNavTabs } from '/@/stores/navTabs' // 后台标签页数据
import { useTerminal } from "/@/stores/terminal" // 终端

const adminInfo = useAdminInfo()
const config = useConfig()
const navTabs = useNavTabs()
const terminal = useTerminal()

console.log("管理员ID是:", adminInfo.id)
console.log("当前语言是:", config.lang.defaultLang)
navTabs.setFullScreen(true) // 将当前标签页设置为全屏
terminal.addTask("version-view.npm") // 执行一个命令

const menuWidth = computed(() => config.menuWidth())
console.log('菜单宽度是:', menuWidth)
</script>
```

## 其他目录
- `\stores\constant`目录，我们在此目录下放置了所有的缓存变量名定义，以便我们可以随时修改缓存变量名，而无需改动具体存储缓存的代码。
- `\stores\interface`目录，我们在此目录下定义了状态商店用到的所有类型`interface`。

## 实现
在`/src/stores/index.ts`文件中，我们创建了`Pinia`，并导入了[pinia-plugin-persistedstate](https://www.npmjs.com/package/pinia-plugin-persistedstate)以实现状态更新后同步保存到浏览器`localStorage`，随后我们在`main.ts`文件内，将创建好的`Pinia`安装到应用上。

### 定义商店
每个商店都应该是单独的文件，有两种语法可以定义商店。
#### 语法一
``` ts
import { defineStore } from 'pinia'

export const useAdminInfo = defineStore('adminInfo', {
    state: (): AdminInfo => {
        return {
            id: 0,
            username: '',
        }
    },
    actions: {
        removeToken() {
            // ...
        },
    },
    persist: {
        // 指定将状态同步保存到localStorage时的key
        key: ADMIN_INFO,
    },
})
```

#### 语法二
``` ts
import { defineStore } from 'pinia'
import { reactive } from 'vue'

export const useAdminInfo = defineStore(
    'adminInfo',
    () => {
        const state = reactive({
            id: 0,
            username: '',
        })

        function removeToken() {
            // ...
        }

        return { state, removeToken }
    },
    {
        // 指定将状态同步保存到localStorage时的key
        persist: {
            key: ADMIN_INFO,
        },
    }
)
```

### 使用商店
``` ts
import { useAdminInfo } from "/@/stores/adminInfo" // 管理员的状态数据
const adminInfo = useAdminInfo()
```