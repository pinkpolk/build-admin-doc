---
pageClass: max-content
---

# 常见问题

## 一、首页提示`请求地址出错:xxxx`
#### 达成条件
- 非[开发环境](https://wonderful-code.gitee.io/guide/other/developerMustSee.html#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)
- 浏览器控制台网络面板的请求状态码为`404`

#### 问题解决
- 如果你使用宝塔面板，请登录宝塔面板->网站->伪静态 中选择`thinkphp`的伪静态规则即可。
- 如果你使用`lnmp.org`的一键安装LNMP环境，请同时参考[官网文档](https://lnmp.org/faq/lnmp-vhost-add-howto.html#rewrite)进行伪静态规则配置
- 以下罗列了`Nginx、Apache、IIS`的URL重写配置方式。

##### [ Nginx ] 配置
``` ini
# Nginx站点配置文件
server {
    listen        80;
    server_name   ba.com;
    root   "D:/WWW/ba.com/public";
    location / {
        # 将 index.html 放在第一位可以实现隐藏 index.html
        index index.html index.php error/index.html;

        # 这一段即为 URL重写规则 请确保存在
        if (!-e $request_filename) {
            rewrite  ^(.*)$  /index.php?s=/$1  last;
            break;
        }
        # 结束
    }

    # 已省略余下代码
}
```

##### [ Apache ] 配置
1. `httpd.conf`配置文件中加载了`mod_rewrite.so`模块
2. 站点对应的`conf`文件中`AllowOverride None`将`None`改为`All`
3. 把下面的内容保存为`.htaccess`文件放到应用入口文件的同级目录下（`public目录`）
``` ini
<IfModule mod_rewrite.c>
  Options +FollowSymlinks -Multiviews
  RewriteEngine On

  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php?s=/$1 [QSA,PT,L]
</IfModule>
```

##### [ IIS ] 配置
1. 安装[url-rewrite](https://www.iis.net/downloads/microsoft/url-rewrite)
2. 站点根目录(public目录)建立`web.Config`文件，写入以下内容：
``` xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <system.webServer>
        <rewrite>
            <rules>
                <rule name="BuildAdmin" stopProcessing="true">
                    <match url="^(.*)$" ignoreCase="false" />
                    <conditions logicalGrouping="MatchAll">
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" ignoreCase="false" negate="true" />
                        <add input="{REQUEST_FILENAME}" matchType="IsFile" ignoreCase="false" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="index.php/{R:1}" appendQueryString="true" />
                </rule>
            </rules>
        </rewrite>
        <defaultDocument>
            <files>
                <clear />
                <add value="index.html" />
                <add value="index.php" />
                <add value="Default.htm" />
                <add value="index.htm" />
                <add value="iisstart.htm" />
            </files>
        </defaultDocument>
    </system.webServer>
</configuration>
```

## 二、导航失败，路由无效
#### CRUD生成的菜单
- 建议您直接部署[开发环境](https://wonderful-code.gitee.io/guide/other/developerMustSee.html#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)。
- 生产环境下(端口8000或无端口)，请先在`web终端`点击重新发布。

#### 自建的菜单规则
1. 请检查`组件路径`是否正确。
2. 后台菜单请将组件放置于`/src/views/backend/`目录内。
3. 前台菜单请将组件放置于`/src/views/frontend/`目录内。

## 三、请求跨域错误
#### 您在[开发环境](https://wonderful-code.gitee.io/guide/other/developerMustSee.html#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)下
1. 开发环境下，请使用`localhost:1818`访问站点，`server`端已经放行该域名，请勿使用`IP`访问。
2. 无需修改`web/.env.development`文件，API请求地址已正确配置（除非你明白你在做什么）。

#### 您在[生产环境](https://wonderful-code.gitee.io/guide/other/developerMustSee.html#%E7%BA%BF%E4%B8%8A%E7%8E%AF%E5%A2%83)下
- `web`与`server`在同一站点，不存在此问题，除非您已经独立部署`web`端，那么请在`config/buildadmin.php`文件中配置允许跨域的域名。

## 四、禁用模块时提示：ENOENT: no such file or directory, open '文件路径'
这是因为禁用模块时会自动删除模块提供的文件，文件丢失后vite报错是正常现象，请刷新浏览器继续使用即可。