---
pageClass: max-content
---

# 运行流程
> 基本引导，破解刚打开代码时的迷茫

## WEB
#### `main.ts`
1. **安装插件**`pinia`、`i18n`、`vue-router`、`element-plus`、`mitt`
2. **路由守护** 在导入`/@/router/index`来安装`vue-router`时，同时注册了`/@/router/static.ts`中的静态路由，安装了全局`loading`，安装了语言包按需加载程序
3. **指令和 el-icon** 注册`/@/utils/directives`中的全部指令，注册`element plus`的全部`icon`
4. **语言包数据初始化** 调用`loadLang`函数加载全局语言包、准备好按需加载语言包文件的句柄
5. **导入全局样式表**`element-plus`基础和暗黑样式表，`/@/styles`中的全部样式表

#### `App.vue`
1. 配置`element-plus`的多语言
2. **加载字体图标**，调用`iconfontInit`函数，动态的添加`link`标签（动态添加标签能取得图标名称列表等信息，供图标选择器使用）
3. **初始化WEB终端** 防止有刷新前正在执行的命令，链接已丢失，直接设置为失败
3. **监听路由变化** 根据路由表中的数据更新浏览器标题

#### 前台
1. 假设用户访问首页`/`
2. 首页为静态路由，用户可以直接访问，绑定组件为`/@/views/frontend/index.vue`
3. 首页代码中使用了`/@/layouts/frontend/components/header.vue`组件，该组件内发送了**前端初始化请求**，获取站点配置信息，动态路由信息、权限节点、站点顶栏菜单数据等
4. 所有前台页面都应该发送**前端初始化请求**，对站点进行初始化
5. 要么使用`第3点`提到的`header`组件，自动发送初始化请求，要么执行`/@/api/frontend/index`中的`index()`请求函数即可

#### 前台会员中心
1. 会员访问`/user`
2. `/user`为静态路由，会员可以直接访问，绑定组件为`/@/layouts/frontend/user.vue`
3. 在`/user`的绑定组件中，调用布局组件，检查了会员登录态，发送了**会员中心初始化请求**，获取会员专属动态路由信息，会员专属权限节点等信息
4. 默认的布局组件`/@/layouts/frontend/container/default.vue`，使用了`/@/layouts/frontend/components/header.vue`组件，如**前台**运行流程所描述，这里单独进行了站点的初始化
5. 会员直接访问`/user/example`时，由于这个路由需要会员登录态进行动态注册，会自动跳转到`/user/loading`等待加载和注册完毕再跳转
6. 用户直接访问会员中心时，会发起两个请求，一个站点初始化，一个会员中心初始化，这种设计是合理和必要的，第二个请求的返回每个会员都可能不一样，而发起第一个请求时会员可能尚未登录

#### 后台
1. `/admin`为静态路由，管理员可以直接访问，绑定组件为`/@/layouts/backend/index.vue`
2. 在`/admin`的绑定组件中，检查登录态，发起了**后台初始化请求**，取得管理员拥有的动态路由，权限节点等信息，并完成了动态路由的注册，后台相关的初始化流程
3. 管理员直接访问`/admin/example`，由于这个路由需要管理员登录态进行动态注册，会自动跳转到`/admin/loading`等待加载和注册完毕再跳转

#### WebNuxt
1. 在`app.vue`发送**前端初始化请求**，获取站点配置信息，权限节点、站点顶栏菜单数据等
2. 利用`plugins`注册指令、`icon`，加载全局语言包，准备语言包按需加载
3. 在`/pages/user.vue`中（会员中心），利用`路由中间件`进行鉴权，配置会员中心的布局为`/layouts/user.vue`，渲染路由出口，加载会员中心菜单后跳转

## Server
1. 一个控制器类往往都有继承于`app/common/controller/Api.php`类，在该类中进行了：IP检查，时区设定，存储/上传初始化，加载语言包。
2. 然后，后台控制器基类为`app\common\controller\Backend`，在该类中进行了`$this->auth`初始化，引入了`Trait类`来实现查询、添加、编辑等功能。
3. 前台控制器基类为`app\common\controller\Frontend`，同样初始化了`$this->auth`，利用`$this->auth`，您可以获取到登录用户的所有数据。
4. 更加详细的介绍，请转到[控制器](https://wonderful-code.gitee.io/senior/server/controller.html)一章。