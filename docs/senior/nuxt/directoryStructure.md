---
pageClass: max-content
---

# 目录结构
```
WebNuxt 工程
├─api（所有接口请求函数即相关）
│  └─  common.ts（公共请求函数）
│          
├─assets（资产）
│  ├─icons（将 svg 自动加载为 Icon 组件的本地图标）
│  │      
│  ├─images （图片）
│  │      
│  ├─script
│  │      buildIcons.ts （构建期间准备本地 Icon 的名称数组供图标选择器使用）
│  │      iconNames.ts （准备好的本地 Icon 名称数组）
│  │      
│  └─scss
│          app.scss 
│          dark.scss（暗黑模式下css变量定义）
│          element.scss （element的css覆盖）
│          index.scss （css入口文件，main.ts只导入它就可以了）
│          mixins.scss（scss mixins与function定义）
│          var.scss（css变量定义）
│          
├─components
│  │  baAside.vue（会员中心公共侧边菜单）
│  │  baFooter.vue（公共底栏）
│  │  baHeader.vue（公共顶栏）
│  │  darkSwitch.vue（暗黑开关组件）
│  │  formItem.vue（表单项，结合baInput）
│  │  icon.vue（字体图标组件，支持加载本地svg、element、awesome）
│  │  loading.vue（一个可供服务端渲染后待客户端激活时使用的 loading 组件）
│  │  
│  └─baInput（输入组件封装：常用+图片/文件上传、数组、城市/图标选择、富文本等封装在了一个组件内）
│          
├─lang（所有语言包及多语言相关）
│  │  autoload.ts（语言包按需加载映射表，比如路由 /user/user 需要同时加载 /user/group 的语言包可在此定义）
│  │  globs-en.ts（全局英文语言包）
│  │  globs-zh-cn.ts（全局中文语言包）
│  │  
│  ├─common（公共页面语言包：`t('dirName.fileName.翻译名')`进行调用）
│  │          
│  └─pages（按需加载的页面语言包：`t('dirName.pageName.翻译名')`进行调用）
│                      
├─layouts（布局）
│      default.vue（默认的空布局）
│      user.vue（会员中心布局）
│      
├─pages（页面）
│              
├─plugins（插件）
│      directives.ts（常用指令）
│      i18n.ts（多语言）
│      icon.ts（Icon相关）
│      route.ts（路由）
│      
├─stores
│  │  globals.ts（全局公用的变量）
│  │  memberCenter.ts（会员中心状态商店）
│  │  siteConfig.ts（系统配置状态商店）
│  │  userInfo.ts（会员信息状态商店）
│  │  
│  ├─constant（常量定义）
│  │      keys.ts（缓存等常用Key名定义）
│  │      
│  └─interface（接口定义）
│          index.ts
│          
├─types（全局类型定义）
│      
├─utils（工具库）
│       common.ts（公共的）
│       dark.ts（暗黑模式相关工具函数）
│       random.ts（随机数、字符串生成）
│       request.ts（网络请求封装）
│       router.ts（路由辅助）
│       validate.ts（表单验证函数）
│ 
│  .editorconfig（IDE风格统一配置）
│  .env.development（开发环境变量定义）
│  .env.production（生产环境变量定义）
│  .eslintignore（eslint忽略）
│  .eslintrc.js（eslint配置）
│  .gitignore（git忽略）
│  .npmrc（npm配置）
│  .prettierignore（prettier忽略）
│  .prettierrc.js（prettier配置）
│  app.vue
│  LICENSE（开源协议）
│  nuxt.config.ts（nuxt配置）
│  package.json
│  README.md
└─ tsconfig.json（ts配置）
```