---
pageClass: max-content
---

# 介绍

我们额外提供了`WebNuxt`工程，来满足`SEO`和`更快的首屏加载`需求；本工程是`BuildAdmin`的[Nuxt](https://nuxt.com/)版本，只含会员前台，不含管理员后台，是仅供会员访问的另外一个站点。

- [WebNuxt代码仓库](https://gitee.com/wonderful-code/build-admin-nuxt) `https://gitee.com/wonderful-code/build-admin-nuxt`
- [Vue官方的SSR文档](https://cn.vuejs.org/guide/scaling-up/ssr.html) `https://cn.vuejs.org/guide/scaling-up/ssr.html`
- [Nuxt文档](https://nuxt.com/docs/getting-started/introduction) `https://nuxt.com/docs/getting-started/introduction`

## 开始
``` ini
# 进入到 buildadmin 项目的根目录
cd buildadmin

# 克隆 web-nuxt 工程到项目根目录，请注意需要克隆到 web-nuxt 目录，以便后续通过后台安装模块
git clone https://gitee.com/wonderful-code/build-admin-nuxt web-nuxt

# 进入到 web-nuxt 目录
cd web-nuxt

# 安装依赖
pnpm install
# yarn install

# 启动服务 -o 表示自动打开浏览器
pnpm dev -o

# 请注意还需启动`BuildAdmin`的服务端
php think run
```

- 安装完成后`web-nuxt`工程的目录路径为`buildadmin/web-nuxt`以方便后续**安装模块**，开发时使用编辑器只打开此目录即可。
- 后续继续使用`git`更新`web-nuxt`工程，若后续在模块市场安装模块时提示您`系统版本不足`，请您在更新系统时，同时更新`web-nuxt`工程。

> `WebNuxt`提供`站点首页`与`会员中心`，它看起来和`BuildAdmin`自带的前台几乎一样，但若您的站点存在`SEO`需求，我们建议您前台的所有开发都基于`WebNuxt`而不是内置的纯`vue`前台，未来`CMS、社区`等模块，我们也会基于本工程进行开发，且涉及到前台的模块，都优先提供`WebNuxt`版本。

## 二次开发
在二次开发之前，请您确保已经理解`服务端渲染`、`客户端激活`等等，它与`纯vue`项目的开发有多个关键概念不相同；\
我们在`Nuxt`工程上，还原了一些`BuildAdmin`原有的设计和代码，其中包括：

- [字体图标](/senior/web/icon.html)`Icon`组件与`Icon`选择器（无`iconfont`图标）。
- [状态管理](/senior/web/stores.html)
- [表单项目组件（formItem）](/senior/web/formItem.html)
- [输入组件（baInput）](/senior/web/baInput.html)
- [表单验证](/senior/web/formValidation.html)
- [CSS/SCSS样式](/senior/web/styles.html) 与原有的设计几乎一致
- [辅助工具/函数](/senior/web/utils.html) 只部分