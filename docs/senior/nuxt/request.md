---
pageClass: max-content
---

# 网络请求
`WebNuxt`的网络请求并未使用`Axios`，而是使用了`Nuxt3`[推荐的方式](https://nuxt.com/docs/getting-started/data-fetching)，我们对`useFetch`进行了封装，它有以下特点：

- 自动携带会员`token`
- 自动携带当前语言
- 可选的 Loading
- 自动错误/成功提示

::: tip
1、在`WebNuxt`的`/api`目录，存放了系统所有的网络请求函数，我们也建议您将封装的请求函数放置于此目录下。\
2、封装的`Http.fetch`无法满足您的使用场景时，当然是可以直接使用`useFetch、$fetch`的，我们对其封装只是为了方便使用。
:::

`Http.fetch`的封装代码位于`/utils/request.ts`，但通常您参考如下方式创建请求即可：
``` ts
Http.fetch(options: FetchOptions<DataT>, config: Partial<FetchConfig> = {}, loading: LoadingOptions = {}): Promise<_AsyncData<ApiResponse<DataT>, FetchError | null>>
```

### 使用示例
- `/api/common.ts`文件
``` ts
/**
 * 初始化请求函数
 */
export function initialize() {
    return Http.fetch({
        url: '/api/index/index',
        method: 'get',
    })
}
```

- 然后就可以在其他文件内
``` ts
import { initialize } from '~/api/common'

const { data } = await initialize()
if (data.value?.code == 1) {
    memberCenter.setStatus(data.value.data.openMemberCenter)
    siteConfig.dataFill(data.value.data.site)
}
```

`Http.fetch`的返回值与`useFetch`是一样的，需要注意的是`code != 1 仍然会进 then`，因为如果抛出异常，将导致服务端渲染时出错；另外有一些其他建议：
1. 请在请求后判断`code == 1`才执行成功的逻辑
2. 请求失败时`code != 1`，会自动弹出接口返回的错误消息，仍然进`then`（可关闭弹窗）
3. 请求出错时，会自动弹出请求错误消息（如404），进`catch`
4. 服务端渲染建议使用`await`的方式发送请求，前台用户点击才发生的请求则随意

### 参数解释
#### 参数一：`options: FetchOptions<DataT>`
`options`参数用于配置`useFetch`的请求参数，如`url、method、query、body、headers`等等，[详细文档](https://nuxt.com/docs/api/composables/use-fetch)，您也可以直接查看类型定义。
``` ts
export function initialize() {
    return Http.fetch({
        url: '/api/index/index',
        method: 'get',
        params: {
            params1: 11
        },
        lazy: true,
    })
}
```

#### 参数二：`config: Partial<FetchConfig> = {}`
`config`参数可以传递以下可用选项：
|选项名|默认值|注释|
|:----:|----|----|
|loading|false|是否开启`loading`层效果|
|reductDataFormat|false|是否开启简洁的数据结构响应(`pick = ['data']`)|
|showErrorMessage|true|是否开启请求错误信息直接提示，如404|
|showCodeMessage|true|是否开启`code!=0`时的信息直接提示（比如操作失败**直接弹窗**提示接口返回的消息，就不需要再写代码弹窗提示了）|
|showSuccessMessage|false|是否开启`code=0`时的信息直接提示|

``` ts
export function initialize() {
    // 定义了接口响应的 data 类型，此操作是可选的
    return Http.fetch<{user: any}>(
        {
            url: indexUrl,
            method: 'get',
            params: {
                params1: 11,
            },
            lazy: true,
        },
        {
            loading: true,
        }
    )
}
```

#### 参数三：`loading: LoadingOptions = {}`
我们封装了`element plus`的`loading`，此参数用于设置`loading`的配置，所有可用配置项，请参考[官方文档](https://element-plus.gitee.io/zh-CN/component/loading.html#%E9%85%8D%E7%BD%AE%E9%A1%B9)。

``` ts
export function initialize() {
    // 定义了接口响应的 data 类型
    return Http.fetch<{user: any}>(
        {
            url: indexUrl,
            method: 'get',
            params: {
                params1: 11,
            },
            lazy: true,
        },
        {
            loading: true,
        },
        {
            text: '正在全力加载中...'
        }
    )
}
```