---
pageClass: max-content
---

# 模块WEB引导程序

自`v1.1.0`起，模块可以向系统`main.ts、App.vue`文件的指定位置插入代码，支持：
1. 在`main.ts、App.vue`文件添加自定义的`import`代码
2. 在`main.ts`文件的`start`函数结尾添加自定义代码
3. 在`App.vue`文件的`onMounted`函数结尾添加自定义代码
4. 这些自定义代码可以在模块安装时自动写入，禁用模块时自动清理

### `webBootstrap.stub`文件

直接将要插入的自定义代码，写到模块根目录的`webBootstrap.stub`文件即可，一共有4个代码块：
``` ini
#main.ts import code start#
console.log('main.ts import 1')
console.log('main.ts import 2')
#main.ts import code end#

#main.ts start code start#
    console.log('main.ts start 1')
    console.log('main.ts start 2')
    console.log('main.ts start 3')
#main.ts start code end#

#App.vue import code start#
console.log('App.vue import 1')
console.log('App.vue import 2')
#App.vue import code end#

#App.vue onMounted code start#
    console.log('App.vue onMounted1')
    console.log('App.vue onMounted2')
    console.log('App.vue onMounted3')
#App.vue onMounted code end#
```

模块安装后`main.ts`的改动如下图所示：

![](/images/senior/module/mainTs.png)

模块安装后`App.vue`的改动如下图所示：

![](/images/senior/module/AppVue.png)