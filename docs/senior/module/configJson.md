---
pageClass: max-content
---

# 模块配置
- 您可以在模块配置中指定模块所需的**NPM依赖**、**Composer依赖**，系统在安装时会自动安装这些依赖。
- 您可以在模块配置中指定模块被禁用时不应被删除的文件，比如此模块覆盖了某个系统核心文件，那么模块被禁用时，禁用程序自动将此核心文件删除时，系统可能会崩溃（虽然系统在安装模块前有自动备份，且在禁用模块时会自动恢复）。

### 模块根目录的 config.json 文件
json不支持注释，所以若要复制以下代码时，请一定删除注释代码。
``` json
{
    // composer依赖列表
    "require": {
        "nelexa/zip": "^3.3",
        "guzzlehttp/guzzle": "^6.3"
    },
    // composer开发环境依赖列表
    "require-dev": {
        "symfony/var-dumper": "^4.2",
        "topthink/think-trace": "^1.1"
    },
    // NPM依赖列表
    "dependencies": {
        "vue-i18n": "~9.1.9",
        "vue-router": "~4.0.11"
    },
    // NPM开发环境依赖列表
    "devDependencies": {
        "@types/lodash": "~4.14.180",
        "@types/node": "~17.0.9"
    },
    // 禁用本模块时，不能删除的文件
    // 如果是系统核心文件：模块禁用时会自动恢复“安装模块时备份的所有冲突文件”，所以以下文件，多数情况会被还原到模块安装之前
    "protectedFiles": ["app/common.php", "app/admin/event.php"]
}
```