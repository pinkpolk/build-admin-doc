# 模块行为事件
`ThinkPHP6`提供了以下系统内置事件：
|事件|描述|参数|
|:----:|:----:|:----:|
|AppInit|应用初始化|无|
|HttpRun|应用开始|无|
|HttpEnd|应用结束|当前响应对象实例|
|LogWrite|日志write方法标签位|当前写入的日志信息|
|RouteLoaded|路由加载完成|无|
|LogRecord|日志记录|无|

模块，可以在[核心控制器](https://wonderful-code.gitee.io/senior/module/coreController.html)文件中，监听以上的`AppInit`事件，而且有它就够了，具体请看下方示例。


## 监听`AppInit`
在核心控制器文件内定义`AppInit`方法即可。
``` php
<?php

namespace modules\test;

class Test
{
    /**
     * 监听 AppInit
     */
    public function AppInit()
    {
        // AppInit 时执行的代码
    }

    /**
     * 安装模块时执行的方法
     */
    public function install()
    {

    }

    // ...
}
```

## 监听其他事件
先定义监听`AppInit`的方法，直接在该方法内注册其他事件的监听，因为`AppInit`是第一个执行的，此时注册监听正合适。
``` php
<?php

namespace modules\test;

use think\facade\Event;
use app\admin\model\UserScoreLog;

class Test
{
    /**
     * 监听 AppInit
     */
    public function AppInit()
    {
        // 监听会员注册成功事件（这个事件是ba定义的）
        Event::listen('user_register_successed', function ($user) {
            UserScoreLog::create([
                'user_id' => $user->id,
                'score'   => 10,
                'memo'    => '感谢注册，赠送10个积分',
            ]);
        });

        // 监听应用开始事件
        Event::listen('HttpRun', function () {
            // 应用开始了
        });
    }

    /**
     * 安装模块时执行的方法
     */
    public function install()
    {

    }

    // ...
}
```

## `BuildAdmin`内置事件列表
|事件|描述|参数|
|:----:|:----:|:----:|
|backendInit|管理员验权标签位|管理员的[鉴权类实例](https://wonderful-code.gitee.io/senior/server/controller.html#%E5%89%8D%E5%90%8E%E5%8F%B0%E6%9D%83%E9%99%90%E7%B1%BB%E5%AE%9E%E4%BE%8B)|
|frontendInit|会员验权标签位|会员的[鉴权类实例](https://wonderful-code.gitee.io/senior/server/controller.html#%E5%89%8D%E5%90%8E%E5%8F%B0%E6%9D%83%E9%99%90%E7%B1%BB%E5%AE%9E%E4%BE%8B)|
|userRegisterSuccessed|会员注册成功标签位|会员数据模型|
|uploadConfigInit|上传信息配置|App $app|
|cacheClearAfter|清理系统缓存后|App $app|
|AttachmentDel|附件删除时|AttachmentModel $attachment|