---
pageClass: max-content
---

# 模块基本信息文件
BuildAdmin通过此文件来识别ZIP包是否为一个正确的模块包文件，它必须存在。

### 模块根目录的 info.ini 文件
``` ini
# 模块唯一标识，若不知您新建的模块标识是否唯一，暂时的，可主动询问群聊管理员
uid = test1
# 模块标题
title = 测试模块
# 模块介绍
intro = 测试模块的介绍
# 模块作者
author = 码上开花
# 模块作者官网ID
authorid = 1
# 模块主页
website = https://www.buildadmin.com
# 模块版本号
version = 1.0.0
# 模块状态，系统自动维护，有此字段即可
state = 0
# 模块在安装时，是否会触发WEB端开发环境的热重载:0=不触发,1=热重载,2=热更新
vitereload = 0
```