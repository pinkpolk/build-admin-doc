---
pageClass: max-content
---

# 模块安装SQL
模块在安装时，会自动在项目数据库中执行此文件内的sql命令。
- 您可以在此文件内，写好建表、改表结构、更新配置等`sql`命令。
- 请在sql命令中使用`__PREFIX__`来代表当前项目的数据表前缀。
- 模块添加的数据表，请以模块唯一标识开头。

### 模块根目录的 install.sql 文件
``` sql
-- 修改系统配置
UPDATE __PREFIX__config SET value='测试1' WHERE name='site_name';

-- 创建一个数据表
CREATE TABLE IF NOT EXISTS `__PREFIX__module_NewTable` (
    `id`  int(10) UNSIGNED NULL AUTO_INCREMENT COMMENT 'ID' ,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='模块添加的数据表';

-- 修改表结构
ALTER TABLE `__PREFIX__module_NewTable` ADD COLUMN `welcome_msg` text COMMENT '欢迎语' AFTER `id`;
```