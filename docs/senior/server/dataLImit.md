---
pageClass: max-content
---

# 数据权限控制
数据权限不同于[菜单规则（路由与权限）](https://wonderful-code.gitee.io/senior/addMenuRule.html)，它是：在单个后台管理功能中，不同管理员只可以查看/编辑有权数据行的权限控制，BuildAdmin 提供全面的数据权限控制功能，在需要开启数据权限的控制器文件内，重写`$dataLimit`属性即可。

``` php
// app/admin/controller/TestBuild.php 文件

// 控制器需继承 Backend 基类
class TestBuild extends Backend
{
    /**
     * 开启数据限制
     */
    protected $dataLimit = false;

    /**
     * 数据限制字段，数据表内必须存在此字段
     */
    protected $dataLimitField = 'admin_id';

    /**
     * 数据限制开启时自动填充字段值为当前管理员id
     */
    protected $dataLimitFieldAutoFill = true;

    // ...
}
```
以上三个控制器属性共同实现了 BuildAdmin 的数据权限控制功能，以下对它们分别解释。

#### dataLimit
- 下文中的`可查`代表有权限，此时也拥有`编辑`和`删除`对应数据行的权限
- 超管可查所有人的数据
- 数据添加人可查自己的添加的数据
- 下表提到的`权限节点`，指的是[菜单规则（路由与权限）](https://wonderful-code.gitee.io/senior/addMenuRule.html)，中的权限节点。

|值|解释|备注|
|:----:|:----:|:----:|
|false|关闭数据权限控制|所有数据所有管理员可查，需拥有该管理功能的权限节点|
|personal|仅限个人|谁添加的谁可查|
|allAuth|拥有某管理员所有的权限时|某管理员拥有添加人的所有权限节点时，可查该添加人的数据|
|allAuthAndOthers|拥有某管理员所有的权限并且还有其他权限时|某管理员拥有添加人的所有权限节点时同时还有另外的权限节点，可查该添加人的数据|
|parent|上级分组中的管理员可查|某管理员在管理员角色组中是添加人的上级，则该管理员可查添加人的数据|
|管理员角色组的`id`号|指定角色组中的管理员可查|指定一个管理员角色组中的管理员们可以查询所有人的数据，分组id请开发者直接在`admin_group`数据表内查得|

#### dataLimitField
- 数据限制字段，此字段保存数据添加人的`id`号，此字段**必须**在对应数据表内存在。
- 当使用`crud`时：因为`crud`还未对数据权限进行额外的兼容，可以先不设计`dataLimitField`字段，生成代码后再添加此字段；也可以设计好此字段，在代码生成之后，找到对应的`popupForm.vue`删除此字段的`FormItem`和`rules`。

#### dataLimitFieldAutoFill
通过将此属性设置为`true`来开启`dataLimitField`字段的自动维护，默认就是开启的。开启后，管理员在添加数据时，会自动在此字段内写入当前管理员的`id`号。