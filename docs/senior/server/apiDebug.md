---
pageClass: max-content
---

# 调试接口
直接访问域名，您会打开站点的`web`端页面；前后端代码分离，但**默认情况下**前后端的部署是不分离的，您需要使用特别的方式来直接访问接口。

### 方案一：`URL`加参
``` ini
# 假设要调试的接口为
http://localhost:8000/admin/index/index

# 加参后为
http://localhost:8000/admin/index/index?server=1
```

### 方案二：请求加`header`
需要加的`header`同样为`server=1`，这也是`BuildAdmin`自身的`web`端请求接口的方式。

### 方案三：`URL`加`index.php`
``` ini
# 假设要调试的接口为
http://localhost:8000/admin/index/index

# 加 index.php 后为
http://localhost:8000/index.php/admin/index/index
```