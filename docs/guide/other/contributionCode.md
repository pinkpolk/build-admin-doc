---
pageClass: max-content
---

> 开源需要大家一起来支持，支持的方式有很多种，比如使用、推荐、写教程、保护生态、贡献代码、回答问题、分享经验等；欢迎您加入我们！

## 操作流程
#### BuildAdmin 的开源仓库地址如下：
- [Gitee仓库](https://gitee.com/wonderful-code/buildadmin)
- [GitHub仓库](https://github.com/build-admin/BuildAdmin)

#### 以 Gitee 仓库为例

##### Fork和克隆
首先进入`BuildAdmin`[仓库网页](https://gitee.com/wonderful-code/buildadmin)，`Fork`仓库到自己的账号下。\
然后克隆Fork的仓库到本地，然后进入`buildadmin`目录

``` bash
git clone https://gitee.com/<yourname>/buildadmin.git
cd buildadmin
```
##### 安装后端依赖和启动安装服务
``` bash
composer install
php think run
```
浏览器访问：[127.0.0.1:8000](http://127.0.0.1:8000/)，根据引导完成安装。

##### 编写代码并测试
- 在本地新增功能或者修复 Bug，进行测试，并将代码`commit`到本地 Git 仓库；
- 请确保`commit`时已经还原安装程序造成的文件修改，主要是以下文件：
``` bash
.env-example
config/buildadmin.php
config/database.php
# 可能会有其他文件，请保证提交的文件仅新增功能或修复Bug产生的文件变更即可

# 还原单个文件的命令
git restore <文件名>
```

- 提交时请参考[提交规范](https://wonderful-code.gitee.io/guide/other/gitCommitSpecification.html)。
- 接下来，推送到自己的远程仓库。
``` bash
git push
```

##### 创建合并请求
进入您`Fork`的 BuildAdmin 仓库的页面，点击 Pull Request 创建合并请求，源分支选择自己仓库的 master，目标分支选择 BuildAdmin 官方仓库的 master，详细描述你的合并请求。

## TS代码规范
- 编辑器安装好`ESlint`和`Prettier`插件/扩展。
- `ESlint`配置后，如果编写代码时不符合规则规范，页面将会出现`红色波浪线`或`黄色波浪线`。
- 执行`npx prettier --write .`命令，可将代码的`引号，分号，换行，缩进`格式化为设定的规范。

## PHP代码规范
- 请参考：https://www.php-fig.org/psr/psr-12/
- 使用 4 个空格作为缩进
- 文件使用`LF`作为换行符
- 必须且只可使用不带 BOM 的 UTF-8编码
- 类名必须以类似`StudlyCaps`形式的大写开头的驼峰命名方式声明
- 方法名称必须符合`camelCase()`式的小写开头驼峰命名规范
- 属性名称和方法名称必须添加访问修饰符（private、protected以及public一定不可使用var）
- 方法的参数列表中，每个逗号后面必须要有一个空格，而逗号前面一定不可有空格，有默认值的参数，必须放到参数列表的末尾。
- 每个`namespace`命名空间声明语句和`use`声明语句块后面，必须插入一个空白行。
- 类的开始花括号`{`必须写在类声明后自成一行，结束花括号`}`也必须写在类主体后自成一行。
- 方法的开始花括号`{`必须写在函数声明后自成一行，结束花括号`}`也必须写在函数主体后自成一行。
- 控制结构的关键字后必须要有一个空格符，而调用方法或函数时则一定不可有，如`if ($a == $b) {...`。
- 控制结构的开始花括号`{`必须写在声明的同一行，而结束花括号`}`必须写在主体后自成一行。
- 控制结构的开始左括号后和结束右括号前，都一定不可有空格符。
- 所有 PHP 文件必须以一个空白行作为结束。
- 纯 PHP 代码文件必须省略最后的?>结束标签，最明显作用是防止结束标签后有字符，造成意外输出。