---
pageClass: max-content
---

> BuildAdmin是一个持续迭代版本、新增功能和修复BUG的系统，您可以参考此文档保持您的BuildAdmin为最新版本。

## Git包
我们推荐您使用Git管理系统的代码，这是目前世界上最好的版本管理工具，没有之一。

#### 更新核心代码
``` bash
# 保存工作现场（将目前还不想提交的但是已经修改的代码保存至堆栈中）
git stash

# 从远程仓库获取最新代码并自动合并到本地
git pull

# pull 命令如果有冲突，先处理冲突，下文有处理冲突示范（您新增的文件不会发生冲突，而框架文件我们更新同时您也更新了该文件才可能冲突）

# 恢复工作现场
git stash pop
```

#### 更新后端依赖
``` bash
composer update -vvv
```

#### 更新前端依赖
``` bash
# ctrl+c 关闭 Vite 开发服务

# 更新前端依赖
pnpm i

# 重新开启 Vite 开发服务
pnpm dev
```

#### 更新数据库
如果`app/admin/buildadmin.sql`文件有更新，请根据[代码仓库中的更新日志](https://gitee.com/wonderful-code/buildadmin/commits/master/app/admin/buildadmin.sql)对比直接修改数据库。

#### 清理系统缓存
后台->右上角垃圾桶图标->清理系统缓存（该操作会删除`runtime/cache目录`）

#### 更新时额外可能会用到的命令
``` bash
# 查看远程仓库信息
git remote -v

# 查看 stash 队列
git stash list

# 清空 stash 队列
git stash clear
```

## 官方更新服务
虽然更新系统非常简单，但任有很多小伙伴完全不了解`GIT`、或单纯的懒，以至于从未对系统进行过更新，现官方推出代更新服务`v1.0`，需要更新协助的小伙伴现在可以直接[加群：751852082](https://jq.qq.com/?_wv=1027&k=c8a7iSk8)后联系管理员，将系统更新到当前最新版本，并处理好所有冲突，付费88，赠送88官网积分，先来先更，感谢您对开源社区的支持~

> 下文提供了一次完整的更新示范，以方便不了解git的小伙伴自助更新框架

## 拉取代码（更新示范第一步）
``` bash
# 保存工作现场
git stash
Saved working directory and index state WIP on master: ******** # 命令输出：保存工作区成功

# 检查远程仓库设置
git remote -v
origin      https://gitee.com/wonderful-code/buildadmin.git (fetch)
origin      https://gitee.com/wonderful-code/buildadmin.git (push)
# PS：笔者这里BuildAdmin远程仓库的名称是origin，你仓库的可能不一样，或者干脆没有地址为`https://gitee.com/wonderful-code/buildadmin.git`的远程仓库设置
# 如果没有该仓库地址，请执行命令添加：git remote add buildadmin https://gitee.com/wonderful-code/buildadmin.git

# 拉取最新代码，buildadmin 为远程仓库名称
git pull buildadmin master
remote: Enumerating objects: 833, done.
remote: Counting objects: 100% (336/336), done.
remote: Compressing objects: 100% (151/151), done.
remote: Total 833 (delta 243), reused 183 (delta 183), pack-reused 497
Receiving objects: 100% (833/833), 265.19 KiB | 385.00 KiB/s, done.
Resolving deltas: 100% (434/434), completed with 72 local objects.
From https://gitee.com/wonderful-code/buildadmin
 * branch            master     -> FETCH_HEAD
 * [new branch]      master     -> buildadmin/master
Auto-merging app/admin/controller/Index.php # 更新已自动合并
Auto-merging app/api/common.php
Auto-merging app/api/controller/Account.php
Auto-merging app/api/controller/User.php
Auto-merging app/api/lang/zh-cn.php
CONFLICT (content): Merge conflict in app/api/lang/zh-cn.php # 冲突！仓库改了这个文件，你本地也改了这个文件，需要手动解决冲突
Auto-merging app/common/library/Auth.php
Auto-merging config/buildadmin.php
Auto-merging extend/ba/module/Manage.php
Auto-merging web/src/api/controllerUrls.ts
Auto-merging web/src/api/frontend/user/index.ts
Auto-merging web/src/utils/axios.ts
CONFLICT (content): Merge conflict in web/src/utils/axios.ts # 冲突！
Auto-merging web/src/views/frontend/user/account/overview.vue
CONFLICT (content): Merge conflict in web/src/views/frontend/user/account/overview.vue # 冲突！
Automatic merge failed; fix conflicts and then commit the result. # 提示你需要手动解决冲突，然后commit
```

## 解决冲突（更新示范第二步）
- 找到以上提示冲突的文件，逐一手动解决冲突。
- 您也可以直接打开`VScode`编辑器，在源代码管理选项卡上，所有的冲突文件被罗列在`合并更改`处，如图所示：\
\
![](/images/guide/other/git-pull-1.png)
\
\
点开第一个冲突文件，可以看到`git`使用`<<<<<<<......>>>>>>>`包裹了冲突部分的代码，并使用`=======`分隔了本地代码和新代码\
\
![](/images/guide/other/git-pull-2.png)
\
\
`VScode`在冲突代码块之上，提供了`Accept Current Change`、`Accept Incoming Change`、`Accept Both Change`、等按钮，可以方便的进行一键合并。非`VScode`用户手动进行合并即可。
\
\
`app/api/lang/zh-cn.php`文件合并前\
\
![](/images/guide/other/git-pull-3.png)\
\
`app/api/lang/zh-cn.php`文件合并后，（合并时请删除`<`、`>`、`=`等符号，只保留正确的代码即可）\
\
![](/images/guide/other/git-pull-4.png)\
\
`src/views/frontend/user/account/overview.vue`文件合并前\
\
![](/images/guide/other/git-pull-5.png)\
\
`src/views/frontend/user/account/overview.vue`文件合并后\
\
![](/images/guide/other/git-pull-6.png)\

## 完成合并（更新示例第三步）
``` bash
# 处理完冲突后，提交代码
git add .
git commit -m 处理冲突

# 恢复工作现场
git stash pop

# 更新后端依赖
composer update

# 更新前端依赖
pnpm install
```

## 调整不兼容更新
框架可能会带来一些不兼容更新，比如：
- 本次更新框架同时更新了`Element-plus`，新版本的`Element-plus`废弃了`el-dialog`的`custom-class`属性，打开站点，F12控制台可见警告；打开编辑器在项目内搜索`custom-class`，将所有`el-dialog`的`custom-class`替换为`class`即可。
- v1.0.6版本框架更新了`buildValidatorData`的传参方式等等，需参考[v1.0.6不兼容更新](/guide/other/incompatibleUpdate/v106.html)对已有代码进行修改。