---
pageClass: max-content
---

# 开发者必看
::: tip
若您已经安装好系统，并准备开始二次开发或体验CRUD、终端等对代码会有变更的操作，我们建议您首先阅读本文档。\
**本文值得仔细研究理解，以免您缺乏基本概念或开发使用的方式方法错误**。
:::

### 开发环境
1. 建议您在本地PC上安装好 BuildAdmin 系统，作为开发环境。
2. 建议您全程使用`php think run`命令启动的服务来进行开发工作，可以选择**不开启**`Nginx、Apache`之类的服务器软件，参[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#%E5%90%AF%E5%8A%A8%E5%AE%89%E8%A3%85%E6%9C%8D%E5%8A%A1)
3. 在安装 BuildAdmin 时您已经填写了系统的数据库资料，需要开启对应的数据库服务，数据库资料被保存在`config/database.php`文件。
4. 在`/web`目录内，执行`npm run dev`命令，在浏览器打开[localhost:1818](http://localhost:1818/)，域名一定是`localhost`（后端已配置它允许跨域）。
5. 开发时，建议开启TP框架的调试模式：找到网站**根目录**的`.env-example`重命名为`.env`。参：[开启调试模式](https://wonderful-code.gitee.io/senior/server/debug.html)。

以上五步曲之后，您修改前端代码，`localhost:1818`的页面会热更新，方便您实时调试。并且api请求会有具体报错信息，CRUD代码生成后，立马就可以看到效果等，接下来，您可以开始查阅[**进阶文档**](https://wonderful-code.gitee.io/senior/)。

::: warning
开发环境执行了两个命令，站点端口是`1818`，接口会响应详细报错消息，并与`Nginx、Apache`无关，提问前请先判断好自己的环境。
:::

### 线上环境（生产环境）
1. 建议删除`/install`目录。
2. 线上环境可以选择**不上传**`/web`目录，前端每次重新发布后，只将`/public/assets 目录`和`/public/index.html 文件`，同步到服务器上即可。
3. 使用`Nginx、Apache`等服务器软件运行站点，而不再是`php think run`，请把`BuildAdmin`当做常规站点，站点的根目录配置为`buildadmin`目录，站点运行目录为`buildadmin/public`，如无运行目录配置项，请直接将根目录配置为`buildadmin/public`。
4. [配置URL重写规则](https://wonderful-code.gitee.io/senior/faq.html#访问首页提示请求地址出错-xxxx-浏览器控制台网络面板的请求状态码为404)
5. 可以选择配置：[隐藏index.html](https://wonderful-code.gitee.io/guide/install/hideIndex.html)。


### 常见问题
#### 为什么开发环境一定是使用`php think run`启动的服务，而不是`Nginx或其他`
- 该服务通过执行一条命令启动，在执行这条命令时，我们能够读取到当前的`环境变量`，以此来实现`WEB终端`的命令执行功能，这条命令启动了站点的`服务端(API服务)`。

#### `web`目录下执行`npm run dev`的意义？
- `vue`项目不同于传统`js、jQuery`项目，开发者所有的改动是需要`编译`的（工程化）；而该命令启动了`Vite`的热更新服务，热更新服务可以实现：`开发环境下无需编译快速查看修改效果`，执行该命令后打开的`localhost:1818`，就是具备热更新、热重载等功能的开发专用站点。

#### 每次改动都需要`重新发布`？
- 错误。如以上的第`2`点所描述，只需要启动热更新服务进行开发工作即可，只在开发工作完成将要上线时，才进行重新发布。