---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

## 您还没有安装`npm`
### Windows系统
- 直接安装`NodeJs`，[下载地址](https://nodejs.org/zh-cn/)
- 使用`下一步`安装法即可
- 安装完成后打开`cmd`或`Windows PowerShell`
- 运行命令`node -v`出现版本号则代表安装成功，如果没有请检查系统环境变量
- 运行命令`npm install -g npm`对`npm`进行升级，因为自带的版本很低

### Linux系统
```
系统较多、暂略...
建议您直接搜索如：Ubuntu安装NodeJs（NodeJs自带npm）
```

## 已安装`npm`但需要升级
- 如果您当前环境下的NPM版本没有达到要求
- 请执行 **npm install -g npm** 对NPM进行升级
- 也可以执行 **npm -g install npm@8.3.0** 升级至指定版本
- 注意`NodeJs`和`npm`通常是相辅相成的，如果升级后出现问题，建议同时[升级NodeJs](https://wonderful-code.gitee.io/guide/install/prepareNodeJs.html)，同时升级完`NodeJs`后，可能会自动重置`npm`到低版本，需要重新进行升级