---
pageClass: max-content
---

> 安装服务`npm build`执行失败

执行`npm build`命令失败了，您可以尝试

1. 删除`根目录/web/node_modules`
2. 推荐使用`pnpm`或`yarn`NPM包管理器，安装`pnpm`命令：`npm install -g pnpm`
3. 手动执行`pnpm install`和`pnpm build`命令，成功后，再打开安装页面右下角的终端，点击`重新发布`按钮
4. 若执行失败，请升级[NodeJs](https://wonderful-code.gitee.io/guide/install/prepareNodeJs.html)和[NPM](https://wonderful-code.gitee.io/guide/install/prepareNpm.html)
5. 若还是执行失败，请根据报错信息排除错误后，再继续进行安装

### 常见错误解决参考