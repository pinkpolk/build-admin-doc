---
pageClass: max-content
---

# Windows从零搭建BuildAdmin

## 必备软件

| 序号 | 软件名称 | 下载链接 | 备注 |
|:----:|:----:|:----:|----|
| 1 |git|[下载](https://git-scm.com/downloads)|全程点击`Next`按钮即可完成安装|
| 2 |NodeJs|[下载](https://nodejs.org/zh-cn/)|全程点击`Next`按钮即可完成安装|
| 3 |phpEnv|[下载](https://www.phpenv.cn/download.html)|[phpEnv安装与配置](/guide/install/windows.md#phpenv安装与配置)|
| 4 |Composer|[下载](https://getcomposer.org/Composer-Setup.exe)|Git克隆的包必需安装`Composer`，<BaFullZip linkText="完整包" />可选，[Composer安装](/guide/install/windows.md#composer安装)|

## 准备开始
打开命令行工具（按下`Win+R`，选择`Windows PowerShell`），请参考注释执行下列命令
``` bash
# 克隆项目
git clone https://gitee.com/wonderful-code/buildadmin.git

# 切换到项目目录
cd buildadmin

# 设置Composer源和下载PHP依赖包，完整包不需要执行这两条命令，git包是需要的
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
composer install

# 启动安装服务
php think run
```
`php think run`命令执行成功后输出类似：
``` bash
ThinkPHP Development server is started On <http://0.0.0.0:8000/>
You can exit with `CTRL-C`
Document root is: D:\WWW\buildadmin\public
```
接下来，请在浏览器访问：[http://127.0.0.1:8000/](http://127.0.0.1:8000/)，根据引导完成安装即可，你也可以[继续查看安装引导说明](/guide/install/webInstallGuide.md)。

::: warning
在安装引导中，如遇WEB终端无法正常使用，请参考：[常见问题](https://wonderful-code.gitee.io/guide/install/start.html#常见问题)
:::

**以下无安装流程步骤**

## phpEnv安装与配置
1. 视喜好随意选择一个[版本](https://www.phpenv.cn/download.html)，使用`下一步安装法`完成安装后`运行软件`

2. `phpEnv`内找到`顶栏菜单->工具->MySql工具->重置密码`，在重置密码的窗口中设置一个数据库密码

![](/images/windows/phpenv-1.png)

3. 点击`启动服务`

![](/images/windows/phpenv-2.png)

4. `phpEnv`内找到蓝色的`数据库`按钮，在打开的窗口中双击`localhost`以链接数据库服务

![](/images/windows/phpenv-3.png)

![](/images/windows/phpenv-4.png)

5. 链接`localhost`数据库服务成功后，右击`localhost`，在打开的菜单中，找到`创建新的`按钮，并创建一个数据库（名称自定）

![](/images/windows/phpenv-5.png)

![](/images/windows/phpenv-6.png)

6. 请记住刚刚创建的`数据库名`、`重置时设定的数据库密码`、数据库账户名为固定的`root`，这些数据在后续安装中都会使用到

## Composer安装
1. 运行`Composer`安装程序
2. 在安装程序的第二步，从`phpEnv`的安装目录中，选择`php.exe`文件的路径，以及勾选`添加到环境变量`\
![](/images/windows/Composer1.png)
3. 一直`下一步`完成安装即可
4. 执行`composer -v`命令，检查是否已准备好\
![](/images/windows/Composer2.png)