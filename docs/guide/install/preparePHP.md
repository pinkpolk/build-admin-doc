---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

# 准备PHP

> BuildAdmin要求PHP版本 >= 7.2.5 安装程序会对此进行检测，若不满足条件，请按以下指南操作

## Windows系统
1. 要求系统已安装PHP，且版本`>=7.2.5`
2. 要求对应PHP已添加到系统环境变量
3. 如果是PHP版本不足，也请参考下面的引导，安装一个版本足够的PHP

### 安装PHP
- **您可以使用任何可能的方式安装`PHP`，比如`宝塔面板、phpEnv、phpStudy、官方下载安装等`**

1. 此处以`phpEnv`为例
2. 前往 [phpEnv官网](https://www.phpenv.cn/download.html) 下载安装包
3. 如Windows下的常规软件安装一样，凭感觉下一步直到安装完成
4. 运行`phpEnv`，在软件首页，点击`启动服务`

### 添加PHP到环境变量
1. 打开`cmd`或`Windows PowerShell`
2. 执行命令`php -v`，如果有提示出来`>=7.2.5`的PHP版本，则您无需继续下面的操作，您的PHP已经准备好了
3. `php -v`提示找不到命令，先找到PHP的安装目录，比如我的`phpStudy`安装在`G:\program files\phpstudy_pro`则PHP目录为`G:\program files\phpstudy_pro\Extensions\php\php7.2.9nts`，该目录内包含`php.exe`文件
4. 按下`Win+R`，输入`sysdm.cpl`，在打开的窗口中切换到`高级`，点击`环境变量`
5. 点击`系统变量`列表中的`Path`，点击`编辑`按钮
6. 点击`新建`，输入`PHP所在目录`，确定保存后，执行`第1-2步`，若无效请`打开新的命令提示符窗口或重启电脑`使环境变量生效

## Linux或Mac系统
- **您可以使用任何可能的方式安装`PHP`，比如`宝塔面板、phpStudy、官方下载编译安装等`**

1. 请自行查找怎么将`PHP`添加到环境变量，相信对于Linux用户来说，这不是难事，多数情况在您安装时，就已经自动添加进去了，那么就不再需要此步骤
2. 打开`终端`
3. 执行命令`php -v`，如果有提示出来`>=7.2.5`的PHP版本，则您的PHP已经准备好了
4. `PHP`需要安装好`fileinfo`和`calendar`扩展，这两个扩展在Linux下通常是没有默认安装的，

### 参考链接
- [PHP 官方安装文档](https://www.php.net/manual/zh/install.php)