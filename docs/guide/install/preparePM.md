---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

# 准备Npm包管理器

请先确保您已安装[NodeJs](https://wonderful-code.gitee.io/guide/install/prepareNodeJs.html)和[NPM](https://wonderful-code.gitee.io/guide/install/prepareNpm.html)

### 各种Npm包管理器安装命令
通常仅需安装以下包管理器中的**其中一个**，请自行选择喜欢的包管理器。
``` bash
# 安装cnpm
npm install cnpm -g --registry=https://registry.npmmirror.com

# 确定cnpm安装成功
cnpm -v
```
``` bash
# 安装yarn
npm install -g yarn

# 确定yarn安装成功
yarn -v
```
``` bash
# 安装pnpm
npm install -g pnpm

# 确定pnpm安装成功
pnpm -v
```
``` bash
# 安装ni
npm install -g @antfu/ni

# 确定ni安装成功
ni -v
```