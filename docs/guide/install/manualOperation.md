---
pageClass: max-content
---

# 手动完成未尽事宜
后台`WEB终端`执行命令失败，需要手动调整至命令能够执行的状态。
1. 请确保您的站点运行在[开发环境](https://wonderful-code.gitee.io/guide/other/developerMustSee.html#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)（后端服务是通过`php think run`启动的）
2. 打开`WEB终端`窗口，查看失败的命令名称，根据下表查得实际执行的**命令**和**目录**
3. 打开/连接您本地PC或服务器的终端，手动执行命令切换到该目录，并执行**实际执行命令**，执行成功后，再前往`WEB终端`点击重试按钮。

注：`php think run`启动的服务内加载了运行环境中所有的`环境变量`，所以理论上，所有本地能正常执行的命令，WEB终端内也可以正常执行，本地失败，则终端必然也会失败。

### `WEB终端`实际命令与目录对照表
|WEB终端命令名称|实际执行命令|执行目录|备注|本地执行失败参考|
|:----:|:----:|:----:|:----:|:----:|
|web-install.npm|npm install|`站点根目录/web`|命令中的`npm`为当前包管理器，可能会有变化|[install执行失败](http://wonderful-code.gitee.io/guide/install/npmInstallFail.html)|
|web-build.npm|npm run build|`站点根目录/web`|命令中的`npm`为当前包管理器，可能会有变化|[build执行失败](http://wonderful-code.gitee.io/guide/install/npmBuildFail.html)|
|composer.update|composer update|`站点根目录`||查看命令窗口的具体报错解决|
|test.npm|npm install|`站点根目录/public/npm-install-test`|命令中的`npm`为当前包管理器，可能会有变化|[install执行失败](http://wonderful-code.gitee.io/guide/install/npmInstallFail.html)|