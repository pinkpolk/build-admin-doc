---
pageClass: max-content
---

# 学习文档
若您作为一位开发者，请在开始之前查阅[BuildAdmin 开发者必看](/guide/other/developerMustSee.html)。\
我们为初学者准备了[BuildAdmin学习线路图](https://wonderful-code.gitee.io/images/BuildAdminStudy.svg)，请点击查看。

## 前端学习文档
- [BuildAdmin 进阶文档](/senior/)
- [TypeScript](https://www.tslang.cn/docs/home.html) `文档地址：https://www.tslang.cn/docs/home.html`
- [Vue3](https://v3.cn.vuejs.org/guide/introduction.html) `文档地址：https://v3.cn.vuejs.org/guide/introduction.html`
- [NPM](https://www.npmjs.cn/) `文档地址：https://www.npmjs.cn/`
- [Axios](http://www.axios-js.com/zh-cn/docs/) `文档地址：http://www.axios-js.com/zh-cn/docs/`
- [Vue Router](https://router.vuejs.org/zh/introduction.html) `文档地址：https://router.vuejs.org/zh/introduction.html`
- [Element Plus](https://element-plus.gitee.io/zh-CN/guide/design.html) `文档地址：https://element-plus.gitee.io/zh-CN/guide/design.html`
- [SCSS](https://www.sass.hk/docs/) `文档地址：https://www.sass.hk/docs/`
- [mitt](https://github.com/developit/mitt) `仓库/使用方式：https://github.com/developit/mitt`
- [Pinia](https://pinia.vuejs.org/introduction.html) `文档地址：https://pinia.vuejs.org/introduction.html`
- [Vue I18n](https://vue-i18n.intlify.dev/introduction.html) `文档地址：https://vue-i18n.intlify.dev/introduction.html`
- [Vite](https://vitejs.cn/guide/) `文档地址：https://vitejs.cn/guide/`
- [vueuse](https://vueuse.org/guide/) `文档地址：https://vueuse.org/guide/`
- [lodash](https://lodash.com/docs/4.17.15) `文档地址：https://lodash.com/docs/4.17.15#concat`
- [wangEditor](https://www.wangeditor.com/v5/getting-started.html) `文档地址：https://www.wangeditor.com/v5/getting-started.html`
- [echarts](https://echarts.apache.org/handbook/zh/get-started/)`文档地址：https://echarts.apache.org/handbook/zh/get-started/`
- [vuePress](https://www.vuepress.cn/guide/)`文档地址：https://www.vuepress.cn/guide/`

## 后端学习文档
- [BuildAdmin 进阶文档](/senior/)
- [PHP](https://www.php.net/manual/zh/) `文档地址：https://www.php.net/manual/zh/`
- [Thinkphp6](https://www.kancloud.cn/manual/thinkphp6_0/1037479) `文档地址：https://www.kancloud.cn/manual/thinkphp6_0`
- [Composer](https://docs.phpcomposer.com/) `文档地址：https://docs.phpcomposer.com/`

## 进阶学习文档
- [Git命令学习](https://oschina.gitee.io/learn-git-branching/) `地址：https://oschina.gitee.io/learn-git-branching/`