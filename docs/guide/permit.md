---
pageClass: max-content
---

# 许可

::: tip Apache2.0 License
[https://gitee.com/wonderful-code/buildadmin/blob/master/LICENSE](https://gitee.com/wonderful-code/buildadmin/blob/master/LICENSE)
- 本项目包含的第三方源码和二进制文件之版权信息另行标注。
:::

::: warning
若您正在设计/开发我们的竞品，并使用了我们的设计或代码，请您主动在项目显目处携带我们的链接，并告知我们。但请放心，开源代码不反对`借鉴`，就如以上协议所述一样，首版著作权号：`2021SR1616095`。
:::

您充分了解并同意：您必须为自己使用本服务及注册帐号下的一切行为负责，包括您所发表的任何内容以及由此产生的任何后果。您应对本服务中的内容自行加以判断，并自行承担因使用内容而引起的所有风险。