---
pageClass: max-content
---

# 技术选型答疑

### 为什么选择php？
1. 根据[W3techs](https://w3techs.com/)报告，截止2022年，在全球排名前1千万的网站中，使用`php`作为服务端语言的站点占比为`77.8%`，且其中`php7~8`版本占比为`76.8%`。
2. `php`非常合适需要快速迭代的`WEB`场景，且它专注于`WEB`，对于中小企业，`php`能让你活着以及给你再战一次的机会。
3. 不足之处当然也有，比如有几家耳熟能详的企业靠`php`起家，但后来进行了技术转型，但这不是你我应该考虑的，能活下来、然后能超越上面提到的全球排名前1千万的`77.8%`的网站再说吧。

### 会不会有其他语言/框架的版本？
未来可能会有Java或纯前端版本，但估计不会有go、Python、node等版本，但是突然某天要学某框架集齐七龙珠也不一定，总的来说请开发者们不要对此有额外期待。

### 为什么不使用Laravel？
1. 大陆使用`ThinkPHP`更多。
2. 若您熟悉`Laravel`，相信上手`tp`非常容易。

### 为什么选择xx而不选择yy
其它并未做过多纠结，总得选一个主要使用的，无法满足所有人需求。