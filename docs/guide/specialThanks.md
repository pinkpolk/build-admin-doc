---
pageClass: max-content
---

# 特别鸣谢

### 感谢 Gitee、GitHub 提供平台

- [Gitee](https://gitee.com/)
- [GitHub](https://github.com/)

### 感谢巨人提供肩膀

**排名不分先后**

- [Thinkphp](http://www.thinkphp.cn/)`http://www.thinkphp.cn/`
- [FastAdmin](https://gitee.com/karson/fastadmin)`本项目灵感起自于与该项目创始人的聊天并得到对方支持，且后续预展开多项合作互持`
- [Vue](https://github.com/vuejs/core)`https://github.com/vuejs/core`
- [vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)`https://gitee.com/lyt-top/vue-next-admin`
- [Element Plus](https://github.com/element-plus/element-plus)`https://github.com/element-plus/element-plus`
- [TypeScript](https://github.com/microsoft/TypeScript)`https://github.com/microsoft/TypeScript`
- [vue-router](https://github.com/vuejs/vue-router-next)`https://github.com/vuejs/vue-router-next`
- [vite](https://github.com/vitejs/vite)`https://github.com/vitejs/vite`
- [Pinia](https://github.com/vuejs/pinia)`https://github.com/vuejs/pinia`
- [Axios](https://github.com/axios/axios)`https://github.com/axios/axios`
- [nprogress](https://github.com/rstacruz/nprogress)`https://github.com/rstacruz/nprogress`
- [screenfull](https://github.com/sindresorhus/screenfull.js)`https://github.com/sindresorhus/screenfull.js`
- [mitt](https://github.com/developit/mitt)`https://github.com/developit/mitt`
- [sass](https://github.com/sass/sass)`https://github.com/sass/sass`
- [wangEditor](https://github.com/wangeditor-team/wangEditor)`https://github.com/wangeditor-team/wangEditor`
- [echarts](https://github.com/apache/echarts)`https://github.com/apache/echarts`
- [vueuse](https://github.com/vueuse/vueuse)`https://github.com/vueuse/vueuse`
- [lodash](https://github.com/lodash/lodash)`https://github.com/lodash/lodash`
- [eslint](https://github.com/eslint/eslint)`https://github.com/eslint/eslint`
- [prettier](https://github.com/prettier/prettier)`https://github.com/prettier/prettier`
- [vuepress](https://github.com/vuejs/vuepress)`https://github.com/vuejs/vuepress`
- [countUp](https://github.com/inorganik/countUp.js)`https://github.com/inorganik/countUp.js`
- [Sortable](https://github.com/SortableJS/Sortable)`https://github.com/SortableJS/Sortable`
- [v-code-diff](https://github.com/Shimada666/v-code-diff)`https://github.com/Shimada666/v-code-diff`

### 支持项目

无需捐赠，如果觉得项目不错，或者已经在使用了，希望你可以去 [Github](https://github.com/build-admin/BuildAdmin) 或者 [Gitee](https://gitee.com/wonderful-code/buildadmin) 帮我们点个 ⭐ Star，这将是对我们极大的鼓励与支持。
