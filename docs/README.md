---
home: true
heroImage: /images/logo.png
heroText: BuildAdmin
tagline: 使用流行技术栈快速创建商业级后台管理系统
---

<template>
    <div>
        <div class="technology-stack">
            <a href="https://www.thinkphp.cn/" target="_blank">
                <img src="https://img.shields.io/badge/ThinkPHP-%3E6.0-brightgreen?color=91aac3&labelColor=439EFD" alt="vue">
            </a>
            <a href="https://v3.vuejs.org/" target="_blank">
                <img src="https://img.shields.io/badge/Vue-%3E3.x-brightgreen?color=91aac3&labelColor=439EFD" alt="vue">
            </a>
            <a href="https://element-plus.gitee.io/#/zh-CN/component/changelog" target="_blank">
                <img src="https://img.shields.io/badge/Element--Plus-%3E2.2-brightgreen?color=91aac3&labelColor=439EFD" alt="element plus">
            </a>
            <a href="https://www.tslang.cn/" target="_blank">
                <img src="https://img.shields.io/badge/TypeScript-%3E4.4-blue?color=91aac3&labelColor=439EFD" alt="typescript">
            </a>
            <a href="https://vitejs.dev/" target="_blank">
                <img src="https://img.shields.io/badge/Vite-%3E2.9-blue?color=91aac3&labelColor=439EFD" alt="vite">
            </a>
            <a href="https://pinia.vuejs.org/" target="_blank">
                <img src="https://img.shields.io/badge/Pinia-%3E2.0-blue?color=91aac3&labelColor=439EFD" alt="vite">
            </a>
            <a href="https://gitee.com/wonderful-code/buildadmin/blob/master/LICENSE" target="_blank">
                <img src="https://img.shields.io/badge/Apache2.0-license-blue?color=91aac3&labelColor=439EFD" alt="license">
            </a>
        </div>
        <div class="hero">
            <p class="action">
                <a href="https://gitee.com/wonderful-code/buildadmin" target="_blank" class="nav-link-git action-button">代码仓库 ✨</a>
                <a href="/guide/" class="nav-link-guide action-button">快速上手 →</a>
            </p>
        </div>
        <div class="features">
            <div class="feature max">
                <h2>🚀 CRUD代码生成</h2>
                <p><del>设计数据表，界面立成。一行命令即可<b>直接生成</b>数据表的增删改查代码</del>，图形化拖拽生成后台增删改查代码，自动创建数据表，<span class="inline-code">大气实用的表格、多达22种表单组件支持、拖拽排序、受权限控制的编辑和删除、支持关联表</span>等等，可为您节省大量开发时间。[ <a target="_blank" href="https://vcdn.buildadmin.com/CRUD-v2.mp4">视频介绍</a> | <a target="_blank" href="https://wonderful-code.gitee.io/senior/oneClickCRUD.html">使用文档</a> ]</p>
            </div>
            <div class="feature max">
                <h2>💥 内置WEB终端</h2>
                <p>我们内置了WEB终端以实现一些理想中的功能，比如：虽然是基于vue3的系统，但你在安装本系统时，并不需要手动执行<code>npm install</code>和<code>npm build</code>命令。且后续本终端将为您提供更多方便、快捷的服务。[ <a target="_blank" href="https://vcdn.buildadmin.com/terminal.mp4">视频介绍</a> | <a target="_blank" href="https://wonderful-code.gitee.io/senior/web/terminal.html">使用文档</a> ]</p>
            </div>
            <div class="feature max">
                <h2>👍 流行且稳定的技术栈</h2>
                <p>除了基于TP6前后端分离架构外，我们的Vue3使用了<code>Setup</code>、状态管理使用了<code>Pinia</code>、并使用了<code>TypeScript、Vite、Element plus</code>等可以为你的知识面添砖加瓦的技术栈。[ <a target="_blank" href="https://wonderful-code.gitee.io/guide/learnDoc.html">相关技术学习文档</a> ]</p>
            </div>
            <div class="feature max">
                <h2>🎨 所见不只是所得</h2>
                <p>后台模块市场还可一键安装数据导出、短信发送、云存储、单页或是纯前端技术栈的学习案例项目等等，随时随地为系统添砖加瓦，系统能够自动维护<code>package.json</code>和<code>composer.json</code>并通过内置终端自动完成模块所需依赖的安装，若您愿意成为模块开发者，模块可以：覆盖系统任何文件或为系统新增文件，您的模块经由官方审核即可上架。[ <a target="_blank" href="https://wonderful-code.gitee.io/senior/module/start.html">模块开发文档</a> | <a target="_blank" href="https://jq.qq.com/?_wv=1027&k=c8a7iSk8">加入我们</a> ]</p>
            </div>
            <div class="feature max">
                <h2>🔀 前后端分离</h2>
                <p><code>web</code>文件夹内包含：<b>干净</b>(不含后端代码)、<b>完整</b>(所有前端代码文件均在此内) 的前端代码文件，对前端开发者友好，作为纯前端开发者，您可以将BAdmin当做学习与资源的社群，本系统可为您准备好案例和模板等所需要的环境，而您只需专注于学习或工作，<b>不需要会任何后端代码！</b> [ 邀您：<a target="_blank" href="https://jq.qq.com/?_wv=1027&k=c8a7iSk8">和我们一起</a> ]</p>
            </div>
            <div class="feature max">
                <h2>✨ 高颜值</h2>
                <p>提供三种布局模式，其中默认布局使用无边框设计风格，它并没有强行填满屏幕的每一个缝然后使用边框线进行分隔，所有的功能版块，都像是悬浮在屏幕上的，同时又将屏幕空间及其合理的利用了 [ <a target="_blank" href="https://demo.buildadmin.com/#/">查看在线演示</a> ]</p>
            </div>
            <div class="feature max">
                <h2>🔐 权限验证</h2>
                <p>可视化的权限管理，然后根据权限动态的注册路由、菜单、页面、按钮(权限节点)、支持无限父子级权限分组、前后端搭配鉴权，自由分派页面和按钮权限 [ <a target="_blank" href="https://wonderful-code.gitee.io/senior/addMenuRule.html">使用文档</a> ]</p>
            </div>
        </div>
        <div class="features mt-0">
            <div class="feature">
                <h2>🌴 数据回收与反悔</h2>
                <p>内置全局数据回收站，并且提供字段级数据修改记录和修改对比，随时回滚和还原，安全且无感。</p>
            </div>
            <div class="feature">
                <h2>🌐 免费开放稳定</h2>
                <p>系统开源，且无需授权即可商业使用，并将持续修改bug和迭代版本，同时系统不会开发pro、plus版本进行收费。</p>
            </div>
            <div class="feature">
                <h2>🔍️ 其他功能</h2>
                <p>角色组/管理员日志、 会员/会员组/会员余额日志、系统配置/附件管理/个人资料管理等等、更多特性等你探索...</p>
            </div>
            <div class="feature">
                <h2>📈 未来可期</h2>
                <p>我们正在持续维护系统，并着手开发更多基础设施模块，按需一键安装，甚至提供开箱即用的各行业完整应用。</p>
            </div>
            <div class="feature">
                <h2>✨ 一举多得</h2>
                <p>后台自适应PC、平板、手机等多种场景的支持，轻松应对各种需求。</p>
            </div>
            <div class="feature">
                <h2>💖 解疑问</h2>
                <p>解答探讨学习和开发中的各种问题，现在入群，您就是鼻祖：<a target="_blank" href="https://jq.qq.com/?_wv=1027&k=c8a7iSk8">751852082</a></p>
            </div>
        </div>
        <div class="page-footer">
            Apache2.0 Licensed | Made by 妙码生花 with ❤️
            <span class="beian" v-if="hostname == 'doc.buildadmin.com' || hostname == 'www.buildadmin.com'">
                <a style="color: #4e6e8e;" target="_blank" href="http://beian.miit.gov.cn/">渝ICP备2020013067号-2</a>
                <span class="beian-gov-cn">
                    <img src="/images/beian.png" width="20" alt="" />
                    <a style="color: #4e6e8e;" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=50011202503447">渝公网安备50011202503447号</a>
                </span>
            </span>
        </div>
    </div>
</template>

<script lang="ts">
export default {
    data() {
        return {
            hostname: ''
        }
    },
    mounted () {
        this.hostname = window.location.hostname
    }
}
</script>

<style scoped>
    .technology-stack {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
    }
    .technology-stack a {
        margin: 0 5px;
    }
    .action-button {
        margin: 0 10px;
        padding: 0.8rem 2rem !important;
        transition: all 0.4s ease !important;
    }
    .nav-link-guide {
        color: #439EFD !important;
        background-color: #fff !important;
        border: 1px solid #3d98fa !important;
    }
    .nav-link-guide:hover {
        border-radius: 25px !important;
        background-color: #f3f4f7 !important;
    }
    .nav-link-git {
        color: #43aeff !important;
        background-color: #fff !important;
        border: 1px solid #43aeff !important;
    }
    .nav-link-git:hover {
        border-radius: 25px !important;
        background-color: #f3f4f7 !important;
    }
    .inline-code {
        color: #303133;
        padding: 0.25rem 0.5rem;
        margin: 0;
        font-size: 0.85em;
        background-color: rgba(27,31,35,0.05);
        border-radius: 3px;
    }
    .feature.max {
        flex-basis: 100%;
        max-width: 100%;
    }
    .mt-0 {
        margin-top: 0 !important;
    }
    .page-footer {
        padding: 2.5rem;
        border-top: 1px solid #eaecef;
        text-align: center;
        color: #4e6e8e;
    }
    .footer.content__footer {
        display: none;
    }
    .beian {
        display: inline-flex;
    }
    .beian-gov-cn {
        display: inline-flex;
        margin-left: 5px;
    }
    .beian-gov-cn img {
        margin-right: 2px;
    }
</style>